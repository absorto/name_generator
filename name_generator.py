#! /usr/bin/python
# -*- coding: utf-8 -*-

from random import choice
import sys

# TODO: adicionar mais nomes variados OU
# Buscar na web nomes reais e embaralhá-los (listagens de Vestibular por exemplo)
# Permitir importar dicionário de nomes externos

def gerador_de_nome(first, second, x):
    """
        Generates random names.

        Arguments:
         - list of first names
         - list of last names
         - number of random names

    """

    names = []
    for i in range(x):
        names.append("{0} {1}".format(choice(first), choice(second)))
    return set(names)


if sys.argv[2] == "masculino":
    nomes = ["João", "Lucas", "Leandro",
               "Thiago", "Ademir", "Eduardo",
                "Nei", "João Pedro", "Jackson", 
               "Paulo"]

elif sys.argv[2] == "feminino":
    nomes = ["Laura", "Luiza", "Maria",
               "Lorena", "Flavia", "Gabriela",
               "Lúcia", "Renata", "Andrea", 
               "Marina"]

sobrenomes = ["Santos", "Cardoso", "Brandão",
              "Rocha", "Schneider", "Mendonça",
              "Barros", "Campos", "Oliveira", 
              "Schmitt"]

# roda com sys.arg
numero = int(sys.argv[1])
nomes = gerador_de_nome(nomes, sobrenomes, numero)
print('\n'.join(nomes))
